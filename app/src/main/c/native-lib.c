#include <jni.h>
#include <malloc.h>
#include <string.h>

#include <ink.h>

#include "../../../../../deck/src/game.h"

JNIEXPORT jlong JNICALL
Java_com_example_deck_Main_newGame(JNIEnv* env, jobject this, jlong ink, jstring dataPath) {
    return game_new(ink);
}

JNIEXPORT jlong JNICALL
Java_com_example_deck_Main_gameUpdate(JNIEnv *env, jobject this, jlong game, jfloat scale_factor) {
    int w, h;
    ink_getViewport(0, &w, &h);

    //float scale_factor = w * (1.f / 600.f);
    //ink_scaling(0, scale_factor);
    game_step(game);

    return 0;
}
