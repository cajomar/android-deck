package com.uiink;
import com.uiink.Ident;
public class I {
public static final Ident bank = new Ident(6305763429184024921L); // bank
public static final Ident card_types = new Ident(388556452271224153L); // card_types
public static final Ident cards = new Ident(7620576327682174051L); // cards
public static final Ident client = new Ident(-8790252288506558484L); // client
public static final Ident color = new Ident(-3071879744171978611L); // color
public static final Ident connect_address = new Ident(6938705967448304889L); // connect_address
public static final Ident connected = new Ident(-1316888999860779522L); // connected
public static final Ident deck = new Ident(2763092244808229906L); // deck
public static final Ident decks = new Ident(9190490944877107616L); // decks
public static final Ident do_spinner = new Ident(9116851693177737977L); // do_spinner
public static final Ident draw = new Ident(-6308025937037672644L); // draw
public static final Ident face_up = new Ident(-7117697994876764302L); // face_up
public static final Ident game = new Ident(245837302133227191L); // game
public static final Ident group = new Ident(-1403027417786958556L); // group
public static final Ident hand = new Ident(-7704550736019254920L); // hand
public static final Ident host = new Ident(-2317973958471432260L); // host
public static final Ident host_address = new Ident(7511124997009486941L); // host_address
public static final Ident id = new Ident(8488222701854180970L); // id
public static final Ident image = new Ident(-6305208100309987583L); // image
public static final Ident list = new Ident(7664278100358914395L); // list
public static final Ident local_order = new Ident(-1620766783044563596L); // local_order
public static final Ident main = new Ident(6571252440867548112L); // main
public static final Ident name = new Ident(5668718655922736026L); // name
public static final Ident play = new Ident(-8930306089152590617L); // play
public static final Ident played = new Ident(5397246563052617219L); // played
public static final Ident player_id = new Ident(-1954986788527715384L); // player_id
public static final Ident players = new Ident(2407571527310747085L); // players
public static final Ident quit_game = new Ident(-4543766966854608475L); // quit_game
public static final Ident reconnect = new Ident(-6443920286252170620L); // reconnect
public static final Ident save = new Ident(-1443019213079314474L); // save
public static final Ident selectSingle = new Ident(-2558286929635146532L); // selectSingle
public static final Ident shuffle = new Ident(3217883329980537519L); // shuffle
public static final Ident slot = new Ident(5027609672776833817L); // slot
public static final Ident type = new Ident(2933420586767109133L); // type
}
