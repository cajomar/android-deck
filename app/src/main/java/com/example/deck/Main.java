package com.example.deck;

import com.uiink.I;
import com.uiink.InkActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.content.res.Configuration;
import android.view.WindowManager;
import java.io.InputStream;
import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

public class Main extends InkActivity {
    private static final String TAG = "Deck";

    static {
        System.loadLibrary("native-lib");
    }

    private float scaleFactor = 1f;
    private long game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        hideNav();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideNav();
        }
    }

    @Override
    public void onInkContextReady() {
        Resources res = getResources();
        final boolean isTablet = (getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
        if (isTablet) {
            scaleFactor = res.getDisplayMetrics().density;
        } else {
            DisplayMetrics dm = res.getDisplayMetrics();
            // Set scaling so that the screen smallest dimension is always 600 ink pixels.
            scaleFactor = Math.min(dm.widthPixels, dm.heightPixels) / 600.f;
        }
        ink.setScale(scaleFactor);

        try {
            InputStream in_s = res.openRawResource(R.raw.gui_assets);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            ink.loadAssetsBinBuffer(b);
        } catch (Exception e) {
            Log.e("INKRUNNER", "Failed to load gui_assets.bin");
            return;
        }
        try {
            InputStream in_s = res.openRawResource(R.raw.gui);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);
            ink.loadGuiBinBuffer(b);
        } catch (Exception e) {
            Log.e("INKRUNNER", "Failed to load gui.bin");
            return;
        }
        game = newGame(ink.getNativePointer(), getFilesDir().getAbsolutePath());
    }

    @Override
    public void onRender(float dt) {
        gameUpdate(game, scaleFactor);
        ink.source(I.game).trigger(I.do_spinner);
    }

    public native long newGame(long ink, String dataPath);
    public native long gameUpdate(long game, float scaleFactor);


    private void hideNav() {
        getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}
